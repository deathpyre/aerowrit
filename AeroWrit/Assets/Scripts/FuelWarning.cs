﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelWarning : MonoBehaviour 
{
	Material myMat = null;
	[SerializeField] Color warningColor = Color.yellow;
	bool lightOn = false;

	bool errorOn = false;



	// Use this for initialization
	void Start ()
	{
		myMat = GetComponent<Renderer>().material;
		SetLight(false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(lightOn)
		{
			float emission = 0f + Mathf.PingPong (Time.time*4f, 6f - 0f);
			var matColor = warningColor;

			matColor *= Mathf.LinearToGammaSpace(emission);
			myMat.SetColor("_EmissionColor",matColor);
		}
		if(errorOn)
		{
			float emission = 0f + Mathf.PingPong (Time.time*15f, 6f - 0f);
			var matColor = Color.red;

			matColor *= Mathf.LinearToGammaSpace(emission);
			myMat.SetColor("_EmissionColor",matColor);
		}
	}

	public void SetLight(bool on)
	{
		if(lightOn == on && on)
			return;
		
		lightOn = on;
		errorOn = false;

		var matColor = Color.black;
		if(on)
			matColor = myMat.color;

		myMat.SetColor("_EmissionColor",matColor);
	}

	public void SetError()
	{
		if(errorOn)
			return;
		errorOn = true;
		lightOn = false;
		var matColor = Color.red;
		matColor *= Mathf.LinearToGammaSpace(6f);
		myMat.SetColor("_EmissionColor",matColor);
	}
}
