﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypingText : MonoBehaviour
{
	public float letterPause = 0.05f;
	public AudioClip openSound = null;

	public AudioSource loopAudioSource = null;
	public AudioSource clipAudioSource = null;
	Text textObj = null;

	int queuedTextIndex = 0;
	List<string> queuedText = new List<string>();

	float timeWaited = 0;
	int charIndex = 0;
	bool typing = false;
	float originalLetterPause = 0;

	// Use this for initialization
	void Awake () 
	{
		textObj = GetComponent<Text>();
		originalLetterPause = letterPause;
	}

	public void AddText(string text)
	{
		queuedText.Add(text);
		StartTyping();
	}

	void StartTyping()
	{
		letterPause = originalLetterPause;
		clipAudioSource.volume = GameController.Instance.volume;
		if(clipAudioSource != null)
			clipAudioSource.PlayOneShot(openSound);
		textObj.text = "";
		typing = true;
		timeWaited = 0;
		charIndex = 0;
	}

	public bool HasTextLeft()
	{
		return queuedText.Count != 0;
	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Escape) && !Input.GetKeyDown(KeyCode.Pause) && !Input.GetKeyDown(KeyCode.LeftWindows) && !Input.GetKeyDown(KeyCode.RightWindows))
		{
			if(!typing)
			{
				queuedTextIndex++;

				if(queuedTextIndex >= queuedText.Count)
				{
					GameController.Instance.SetTextOn(false);
					letterPause = originalLetterPause;
					loopAudioSource.Stop();
					queuedText.Clear();
					queuedTextIndex = 0;
					return;
				}
				else
				{
					StartTyping();
				}
			}
			else
			{
				letterPause = 0;
				loopAudioSource.Stop();
			}

		}

		if(typing)
		{
			timeWaited += Time.deltaTime;
			if(timeWaited > letterPause)
			{
				if(charIndex == 0)
				{
					loopAudioSource.volume = GameController.Instance.volume;
					loopAudioSource.Play();
				}
				var charArray = queuedText[queuedTextIndex].ToCharArray();
				textObj.text += charArray[charIndex];
				timeWaited = 0;

				if(charArray[charIndex] == ' ' || charArray[charIndex] == '.')
				{
					//if(audioSource != null)
					//	audioSource.PlayOneShot(sound);
				}
				if(charArray[charIndex] == '<')
				{
					var closeCount = 0;
					charIndex++;
					while(closeCount < 2)
					{
						textObj.text += charArray[charIndex];
						if(charArray[charIndex] == '>')
							closeCount++;
						charIndex++;
					}
				}
				else
					charIndex++;
				
				if(charIndex > charArray.Length-1)
				{
					loopAudioSource.Stop();
					typing = false;
				}
			}
		}
	}
}
