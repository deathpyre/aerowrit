﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterManager : MonoBehaviour
{
	[SerializeField] Transform waterPrefab = null;
	List<Transform> waters = new List<Transform>();
	[SerializeField] PlaneManager planeManager = null;
	[SerializeField] RingManager ringManager = null;


	public Vector2 size = new Vector2(5000,5000);
	// Use this for initialization
	void Start ()
	{
		for(int i = 0; i < 9; i++)
		{
			var column = (int)(i % 3);
			var row = (int)(i / 3);

			column -= 1;
			row -= 1;
			var spawnedWater = GameObject.Instantiate(waterPrefab, Vector3.zero, Quaternion.identity, transform) as Transform;

			spawnedWater.localPosition = new Vector3(size.x * column, 0 , size.y * row);
			waters.Add(spawnedWater);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		var planeLocalPos = transform.InverseTransformPoint(planeManager.transform.position);
		if(Mathf.Abs(planeLocalPos.x) > size.x/2f)
		{
			var xSign = Mathf.Sign(planeLocalPos.x);
			transform.position += new Vector3(size.x,0,0) * xSign;
		}
		else if(Mathf.Abs(planeLocalPos.z) > size.y/2f)
		{
			var zSign = Mathf.Sign(planeLocalPos.z);
			transform.position += new Vector3(0, 0, size.y) * zSign;
			ringManager.MoveRow((int)zSign);
		}
	}
}
