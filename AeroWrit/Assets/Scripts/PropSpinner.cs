﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpinner : MonoBehaviour
{
	[SerializeField] Vector3 spinVelocity = Vector3.zero;
	[SerializeField] PlaneManager planeManager = null;
	
	// Update is called once per frame
	void Update ()
	{
		if(planeManager.engineOn)
			transform.localEulerAngles += spinVelocity * Time.deltaTime;
	}
}
