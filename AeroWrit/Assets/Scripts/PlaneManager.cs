﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneManager : MonoBehaviour
{
	[Header("Flight Info")]
	[SerializeField] float speed = 100;
	public float fuel = 2000;
	public float maxFuel = 3000;
	public bool engineOn = true;
	[SerializeField] float engineFuelConsumption = 0.1f;
	//[SerializeField] float engineAcceleration = 4000;
	[SerializeField] float maxSpeed = 300000;
	[SerializeField] Vector3 velocity = Vector3.zero;
	[SerializeField] Vector3 forward = Vector3.zero;
	[SerializeField] Vector3 inputBuffer = Vector3.zero;
	[Header("Turn Rates")]
	[SerializeField] Vector2 yawDegPerSecond = new Vector2(6f, 15f);
	[SerializeField] Vector2 pitchDegPerSecond = new Vector2(6f, 15f);
	[SerializeField] Vector2 rollDegPerSecond = new Vector2(6f, 15f);
	[Header("Audio")]
	[SerializeField] AudioSource audio3d = null;
	[SerializeField] AudioSource uiAudioSource = null;
	[SerializeField] AudioClip explosion = null;
	[SerializeField] AudioClip ringGet = null;
	[SerializeField] AudioSource engineAudio = null;
	[SerializeField] AudioClip engineDie = null;
	[Header("Extras")]
	[SerializeField] FuelWarning fuelWarning = null;
	[SerializeField] Transform particles = null;
	public ChatLogManager logManager = null;
	public List<BoxCollider> colliders = new List<BoxCollider>();

	float resistance = 1000f;
	bool crashed = false;
	bool crashing = false;
	float crashTime = 0;

	float totalMeters = 0;
	float totalTime = 0;

	float enginePitchPercent = 0;
	float enginePitchAdditional = 0.6f;
	float engineRampTime = 0.6f;

	public bool lockedFuel = false;

	Vector3 lastPosition = Vector3.zero;
	Quaternion lastRotation = Quaternion.identity;
	// Use this for initialization
	void Start ()
	{
		lastPosition = transform.position;
		fuelWarning.SetLight(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(crashed)
		{
			particles.position = transform.position;
			return;
		}
		totalTime += Time.deltaTime;
		if(crashing)
		{
			crashTime += Time.deltaTime;
			if(crashTime > 0.03f)
			{
				engineAudio.Stop();
				engineOn = false;
				crashed = true;
				GameController.Instance.PlayerDied(totalTime, totalMeters);
				return;
			}
		}

		var currentPosition = transform.position;

		if(lastPosition == Vector3.zero)
			lastPosition = currentPosition;
		else
		{
			var distance = Vector3.Distance(currentPosition,lastPosition);
			totalMeters += distance;
		}


		if(!crashing && !crashed)
		{
			foreach(var collider in colliders)
			{
				if(collider.bounds.Intersects(new Bounds(Vector3.zero, new Vector3(9999999,0.1f,9999999))))
				{
					crashing = true;
					crashTime = 0;
					audio3d.PlayOneShot(explosion);
				}
			}
		}

		forward = transform.forward;

		//var lastMetersPerSecond = (currentPosition - lastPosition) * Time.deltaTime;
		//velocity = MpsToMph(lastMetersPerSecond);
		if(engineOn)
		{
			//velocity += MpsToMph(forward * (engineAcceleration * Time.deltaTime));//new Vector3(0,0,engineAcceleration);
			if(!lockedFuel)
				fuel -= engineFuelConsumption;
			if(fuel <= 0)
			{
				engineAudio.Stop();
				engineAudio.PlayOneShot(engineDie);
				fuel = 0;
				logManager.AddText("YOU RAN OUT OF FUEL!", false, true);
				engineOn = false;
				LevelOut();
				inputBuffer.y += 20;
			}
			AdjustByFuel();
		}

		//velocity = Vector3.ClampMagnitude(velocity, maxSpeed);

		//velocity = Vector3.RotateTowards(velocity, transform.forward*velocity.magnitude, Mathf.PI/6 * Time.deltaTime, 4335345435435435);*/


		//if(engineOn)
		velocity = forward * maxSpeed;

		speed = velocity.magnitude / 1000f;
		transform.position += MphToMps(velocity) * Time.deltaTime;

		if(inputBuffer != Vector3.zero)
		{
			enginePitchPercent += Time.deltaTime / engineRampTime;
		}
		else
		{
			enginePitchPercent -= Time.deltaTime / engineRampTime;
		}

		enginePitchPercent = Mathf.Clamp(enginePitchPercent, 0 ,1);

		if(fuel <= 0)
			engineAudio.pitch = 1;
		else
			engineAudio.pitch = Mathf.SmoothStep(1, 1 + enginePitchAdditional, enginePitchPercent);

		ProcessInputs();

		lastPosition = currentPosition;
	}

	float ApplyResistance(float value)
	{
		if(value == 0 || Mathf.Abs(value) <  MpsToMph(resistance* Time.deltaTime)) // If velocity is less than air resistence clear it
			value = 0;
		else
		{
			var sign = Mathf.Sign(value);
			value -= MpsToMph(sign*resistance* Time.deltaTime);
		}
		return value;
	}

	public void Inputs(Vector3 inputs)
	{
		if(barrelRolling)
			return;
		
		if(!engineOn)
			inputs.y = 0;
		inputBuffer += inputs;
	}

	bool barrelRolling;
	public void BarrelRoll()
	{
		if(!barrelRolling)
		{
			ClearInputs();
			barrelRolling = true;
			inputBuffer.z = -360;
		}
	}

	public void LevelOut()
	{
		if(barrelRolling)
			return;
		
		inputBuffer.y = 0;

		if(transform.localEulerAngles.x > 180)
			inputBuffer.y = 360-transform.localEulerAngles.x;
		else
			inputBuffer.y = -transform.localEulerAngles.x;

	}

	public void ClearInputs()
	{
		inputBuffer.x = 0;
		inputBuffer.y = 0;
	}

	void ProcessInputs()
	{
		var yaw = inputBuffer.x;
		var pitch = inputBuffer.y;
		var roll = inputBuffer.z;

		var percentSpeed = Mathf.Clamp(Mathf.Abs(yaw),0,90) / 90f;
		var allowedYaw = Mathf.Lerp(yawDegPerSecond.x, yawDegPerSecond.y, percentSpeed);
		//allowedYaw *= Mathf.Sign(yaw);


		yaw = Mathf.Clamp(yaw, -allowedYaw, allowedYaw);
		if(Mathf.Abs(yaw) > 0.7f)
			yaw *= Time.deltaTime;
		inputBuffer.x -= yaw;



		percentSpeed = Mathf.Clamp(Mathf.Abs(pitch),0,90) / 90f;
		var allowedPitch = Mathf.Lerp(pitchDegPerSecond.x, pitchDegPerSecond.y, percentSpeed);
		//actualPitch *= Mathf.Sign(pitch);

		pitch = Mathf.Clamp(pitch, -allowedPitch, allowedPitch);
		if(Mathf.Abs(pitch) > 0.7f)
			pitch *= Time.deltaTime;
		inputBuffer.y -= pitch;

		var allowedRoll = 60f;

		roll = Mathf.Clamp(roll, -allowedRoll, allowedRoll);
		if(Mathf.Abs(roll) > 0.7f)
			roll *= Time.deltaTime;
		else
			barrelRolling = false;
		inputBuffer.z -= roll;

		transform.localRotation *= Quaternion.Euler(new Vector3(pitch, 0, roll));

		transform.eulerAngles += new Vector3(0,yaw,0);
	}

	public void GiveFuel(float amount)
	{
		uiAudioSource.PlayOneShot(ringGet);
		fuel += amount;

		fuel = Mathf.Clamp(fuel, 0, maxFuel);

		AdjustByFuel();
	}

	public void GiveMaxFuel()
	{
		uiAudioSource.PlayOneShot(ringGet);

		fuel = maxFuel;
		AdjustByFuel();
	}

	void AdjustByFuel()
	{
		if(crashed || crashing)
			return;
		
		if(fuel > 0)
			engineOn = true;

		if(fuel > 700)
			fuelWarning.SetLight(false);
		else if(fuel <= 100)
			fuelWarning.SetError();
		else
			fuelWarning.SetLight(true);
		
	}

	Vector3 MpsToMph(Vector3 metersPerSec)
	{
		return new Vector3(MpsToMph(metersPerSec.x), MpsToMph(metersPerSec.y), MpsToMph(metersPerSec.z));
	}

	float MpsToMph(float metersPerSec)
	{
		return metersPerSec * 3600f;
	}

	Vector3 MphToMps(Vector3 metersPerHour)
	{
		return new Vector3(MphToMps(metersPerHour.x), MphToMps(metersPerHour.y), MphToMps(metersPerHour.z));
	}

	float MphToMps(float metersPerHour)
	{
		return metersPerHour / 3600f;
	}
}
