﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingManager : MonoBehaviour
{
	public static RingManager Instance = null;

	[SerializeField] WaterManager waterManager = null;
	[SerializeField] Transform ringPrefab = null;

	List<RingDetails> rings = new List<RingDetails>();
	int ringsPerSquare =  15;
	public Vector2 heightRange = new Vector2(40, 500);
	// Use this for initialization
	void Start ()
	{
		Instance = this;

		for(int i = 0; i < 9; i++)
		{
			var currentColumn = (int)(i % 3);
			var currentRow = (int)(i / 3);

			for(int j = 0; j < ringsPerSquare; j++)
			{
				var spawnedRing = SpawnRing();
				spawnedRing.name = "Ring "+i.ToString()+"-"+j.ToString();
				var relativeColumn = currentColumn-1;
				var relativeRow = currentRow-1;

				RandomizeTransformPosition(spawnedRing, relativeRow, relativeColumn);

				var details = new RingDetails()
				{
					script = spawnedRing.GetComponent<RingScript>(),
					column = currentColumn,
					row = currentRow
				};
				rings.Add(details);
			}
		}
	}

	public Transform SpawnRing()
	{
		var spawnedRing = GameObject.Instantiate(ringPrefab, transform);
		return spawnedRing;

	}

	void RandomizeTransformPosition(Transform ring, int relativeRow, int relativeColumn)
	{
		var halfSizeX = waterManager.size.x/2f;
		var halfSizeY = waterManager.size.y/2f;

		var randomX = Random.Range(-halfSizeX, halfSizeX);
		var randomZ = Random.Range(-halfSizeY, halfSizeY);
		var randomY = Random.Range(heightRange.x, heightRange.y);

		ring.position = waterManager.transform.position + new Vector3(relativeColumn * waterManager.size.x + randomX, randomY , relativeRow * waterManager.size.y+randomZ);


		ring.eulerAngles = new Vector3(0,Random.Range(0,360f));
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void MoveRow(int direction)
	{
		for(int i = 0; i < rings.Count; i++)
		{
			rings[i].row -= direction;
			bool regenerate = false;
			if(rings[i].row > 2)
			{
				rings[i].row = 0;
				regenerate = true;
			}
			if(rings[i].row < 0)
			{
				rings[i].row = 2;
				regenerate = true;
			}

			if(regenerate)
			{
				var relativeColumn = rings[i].column-1;
				var relativeRow = rings[i].row-1;
				rings[i].script.gameObject.SetActive(true);
				RandomizeTransformPosition(rings[i].script.transform, relativeRow, relativeColumn);
			}
		}
	}
}

class RingDetails
{
	public RingScript script = null;
	public int column = 0;
	public int row = 0;
}
