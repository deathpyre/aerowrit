﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour 
{
	public GameObject creditsPage = null;
	public Button tutorialButton = null;
	// Use this for initialization
	void Start ()
	{
		creditsPage.SetActive(false);
		tutorialButton.interactable = PlayerPrefs.GetInt("CompletedTutorial",0) == 1;
	}

	public void FinishedStartFade()
	{
		SceneManager.LoadScene("World");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(creditsPage.activeSelf && Input.anyKey)
		{
			creditsPage.SetActive(false);
		}
	}

	public void StartPressed()
	{
		Cursor.visible = false;
		BlackoutScreen.Instance.FadeDown(FinishedStartFade);
	}

	public void Credits()
	{
		creditsPage.SetActive(true);
	}

	public void ResetTutorial()
	{
		PlayerPrefs.SetInt("CompletedTutorial",0);
		var nowSet = PlayerPrefs.GetInt("CompletedTutorial",0);
		tutorialButton.interactable = nowSet == 1;
	}

	public void Quit()
	{
		Application.Quit();
	}
}
