﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingScript : MonoBehaviour 
{
	public static float fuelAmount = 500;
	PlaneManager plane = null;
	[SerializeField] BoxCollider myCollider = null;

	[SerializeField] bool objectiveRing = false;
	// Use this for initialization
	void Start ()
	{
		plane = GameObject.FindObjectOfType<PlaneManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(objectiveRing)
			transform.localEulerAngles += new Vector3(0,10f,0) * Time.deltaTime;
		
		if(Vector3.Distance(plane.transform.position, transform.position) < transform.localScale.x + 20)
		{
			//Debug.LogWarning("CHECKING");
			foreach(var planeCollider in plane.colliders)
			{
				if(myCollider.bounds.Intersects(planeCollider.bounds))
				{
					gameObject.SetActive(false);
					//Debug.LogError("HIT");
					if(!objectiveRing)
						plane.GiveFuel(fuelAmount);
					else
					{
						plane.GiveMaxFuel();
						GameController.Instance.CollectedTarget();
					}
				}
			}
		}
	}
}
