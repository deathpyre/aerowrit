﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackoutScreen : MonoBehaviour
{
	public static BlackoutScreen Instance = null;
	bool fading = false;
	[SerializeField] float percent = 0;
	bool up = true;

	public Image myImage = null;


	public delegate void TestDelegate(); // This defines what type of method you're going to call.
	public TestDelegate m_methodToCall; // This is the variable holding the method you're going to call.

	// Use this for initialization
	public BlackoutScreen ()
	{
		Instance = this;
	}

	float fadeMultiplier = 1;
	public void FadeDownFast(TestDelegate callback)
	{
		fadeMultiplier = 13;
		FadeDown(callback);
	}

	public void FadeDown(TestDelegate callback)
	{
		gameObject.SetActive(true);
		fading = true;
		percent = 0;
		up = false;
		m_methodToCall = callback;
	}
	public void FadeUp(TestDelegate callback)
	{
		gameObject.SetActive(true);
		fading = true;
		percent = 1;
		up = true;
		m_methodToCall = callback;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(fading)
		{
			var currentColor = myImage.color;

			var delta = (up ? -0.1f : 0.1f) * Time.deltaTime * (12+fadeMultiplier);
			percent += delta;
			currentColor.a = percent;
			myImage.color = currentColor;

			if(percent >= 1 || percent <= 0)
			{
				fading = false;
				m_methodToCall();
				//gameObject.SetActive(false);
				fadeMultiplier = 1;
			}
		}
	}
}
