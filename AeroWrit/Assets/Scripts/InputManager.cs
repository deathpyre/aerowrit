﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
	[SerializeField] Vector3 input = Vector2.zero;
	[SerializeField] InputField inputField = null;
	[SerializeField] Sway characterManager = null;
	[SerializeField] AudioSource uiAudioSource = null;
	[SerializeField] AudioClip enterSound = null;
	[SerializeField] AudioClip incorrectSound = null;

	List<string> yaw = new List<string>(){"yaw","turn","go"};
	List<string> pitch = new List<string>(){"pitch","turn","go"};
	List<string> stop = new List<string>(){"stop","cancel","end"};
	List<string> altimeter = new List<string>(){"altimeter","altitude","height"};




	List<string> instrumentNames = new List<string>(){"Altimeter/Height","Compass","Fuel"};
	List<string> commandNames = new List<string>(){"Left/Right","Up/Down","Stop","Level","Lean Left/Right"};

	PlaneManager planeManager = null;
	// Use this for initialization
	void Start ()
	{
		planeManager = GetComponent<PlaneManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!inputField.isFocused)
			inputField.ActivateInputField();


		input = Vector2.zero;
		if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			var thatInput = planeManager.logManager.GetNextInput();
			if(thatInput != "")
				inputField.text = thatInput;
		}
		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			var thatInput = planeManager.logManager.GetPreviousInput();
			if(thatInput != "")
				inputField.text = thatInput;
		}

		//input.x += Input.GetAxis("Horizontal");
		//input.y += Input.GetAxis("Vertical");

		//

		if(!GameController.Instance.showingTyper && (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)))
		{
			bool validInput = false;
			var text = inputField.text.ToLower();

			var result = GameController.Instance.FilterText(text);
			if(!result)
			{
				uiAudioSource.PlayOneShot(incorrectSound);
				text = "";
			}

			var remaining = "";
			if(text.Contains("left"))
			{
				remaining = text.Replace("left","").Trim();
				float parsed = 0;
				float.TryParse(remaining, out parsed);
				input.x = -parsed;
			}

			if(text.Contains("right"))
			{
				remaining = text.Replace("right","").Trim();
				float parsed = 0;
				float.TryParse(remaining, out parsed);
				input.x = parsed;
			}

			if(input.x != 0)
				OutputText(text, true);

			validInput = validInput || input.x != 0;


			if(text.Contains("up"))
			{
				remaining = text.Replace("up","").Trim();
				float parsed = 0;
				float.TryParse(remaining, out parsed);
				input.y = -parsed;
			}

			if(text.Contains("down"))
			{
				remaining = text.Replace("down","").Trim();
				float parsed = 0;
				float.TryParse(remaining, out parsed);
				input.y = parsed;
			}
			if(input.y != 0)
				OutputText(text, true);

			validInput = validInput || input.y != 0;
			if(validInput)
				planeManager.Inputs(input);







			var foundIndex = CheckForWord(text, stop);
			if(foundIndex != -1)
			{
				validInput = true;
				OutputText(text, true);
				planeManager.ClearInputs();
			}

			if(text.Contains("lean"))
			{
				remaining = text.Replace("lean","").Trim();
				if(remaining.Contains("left"))
				{
					characterManager.Lean(1);
					validInput = true;
					OutputText(text, true);
				}
				if(remaining.Contains("right"))
				{
					characterManager.Lean(-1);
					validInput = true;
					OutputText(text, true);
				}
			}

			//if(text.Contains("check"))
			//{
				//var remaining = text.Replace("check","").Trim();
			if(text.Contains("fuel"))
			{
				validInput = true;
				var fuelLevel = "Fuel level: "+Mathf.RoundToInt(planeManager.fuel).ToString("N0");
				OutputText(text, true);
				OutputCommand(fuelLevel, false);
			}
			if(text.Contains("compass"))
			{
				validInput = true;
				OutputText(text, true);
				var rotation = transform.eulerAngles.y;

				var direction= AngleToCompass(rotation);
				OutputCommand("The compass reads: "+direction, false);
			}
			foundIndex = CheckForWord(text, altimeter);
			if(foundIndex != -1)
			{
				validInput = true;
				OutputText(text, true);
				OutputCommand("The altimeter reads: "+transform.position.y.ToString("N0")+"m", false);
			}
			//}
			if(text.Contains("instruments"))
			{
				validInput = true;
				OutputText(text, true);
				for(int i = 0; i < instrumentNames.Count; i++)
				{
					OutputCommand((i+1).ToString()+". "+instrumentNames[i], false);
				}
			}
			if(text.Contains("map"))
			{
				validInput = true;
				OutputText(text, true);
				var currentPos = transform.position;
				currentPos.y = GameController.Instance.targetPosition.y;

				Vector3 targetDir = GameController.Instance.targetPosition - currentPos;
				Vector3 forward = Vector3.forward;
				float angle = Vector3.SignedAngle(targetDir, forward, -Vector3.up);
				var direction = AngleToCompass(angle);
				OutputCommand("The target is: "+direction, false);
				OutputCommand("Distance: "+(targetDir.magnitude/1000).ToString("N2")+"km", false);
				OutputCommand("Altitude: "+GameController.Instance.targetPosition.y.ToString("N0")+"m", false);
			}

			if(text.Contains("barrel roll"))
			{
				validInput = true;
				OutputText(text, true);
				planeManager.BarrelRoll();
			}

			if(text.Contains("level"))
			{
				validInput = true;
				OutputText(text, true);
				planeManager.LevelOut();
			}

			if(text.Contains("commands"))
			{
				validInput = true;
				OutputText(text, true);
				for(int i = 0; i < commandNames.Count; i++)
				{
					OutputCommand((i+1).ToString()+". "+commandNames[i], false);
				}
			}

			if(text.Contains("help"))
			{
				validInput = true;
				OutputText(text, true);
				OutputCommand("You can type the following for more info:",false);
				OutputCommand("1. Instruments",false);
				OutputCommand("2. Commands",false);
			}

			//if(text.Contains("heading"))
			//{
			//	validInput = true;
			//	OutputText(text, true);
			//}

			/*if(text.Contains("distance"))
			{
				validInput = true;
				OutputText(text, true);
				//planeManager.LevelOut();
			}

			if(text.Contains("level"))
			{
				validInput = true;
				OutputText(text, true);
				//planeManager.LevelOut();
			}*/


			if(validInput)
			{
				uiAudioSource.PlayOneShot(enterSound);
			}
			else
			{
				if(!string.IsNullOrEmpty(text))
				{
					uiAudioSource.PlayOneShot(incorrectSound);
					OutputText(text, validInput);
				}
			}


			//inputText.text = "";
			//inputField.ForceLabelUpdate();
			inputField.text = "";
			planeManager.logManager.ClearInputIndex();
		}
	}

	int CheckForWord(string textToSearch, List<string> currentList)
	{
		for(int i = 0; i < currentList.Count; i++)
		{
			if(textToSearch.Contains(currentList[i]))
				return i;
		}
		return -1;
	}

	void OutputText(string text, bool asValid)
	{
		planeManager.logManager.AddText(text, asValid, true);
	}

	void OutputCommand(string text, bool asValid)
	{
		planeManager.logManager.AddText(text, asValid, false);
	}

	string AngleToCompass(float angle)
	{
		while(angle < 0)
			angle += 360;
		
		var range = 22.5f;
		string direction = "";
		if(angle >= 360 - range || angle <= 0 + range)
		{
			direction = "N";
		}
		else if(angle >= 45 - range && angle <= 45 + range)
		{
			direction = "NE";
		}
		else if(angle >= 90 - range && angle <= 90 + range)
		{
			direction = "E";
		}
		else if(angle >= 135 - range && angle <= 135 + range)
		{
			direction = "SE";
		}
		else if(angle >= 180 - range && angle <= 180 + range)
		{
			direction = "S";
		}
		else if(angle >= 225 - range && angle <= 225 + range)
		{
			direction = "SW";
		}
		else if(angle >= 270 - range && angle <= 270 + range)
		{
			direction = "W";
		}
		else if(angle >= 315 - range && angle <= 315 + range)
		{
			direction = "NW";
		}

		return direction;
	}
}
