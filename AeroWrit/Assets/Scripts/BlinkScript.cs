﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkScript : MonoBehaviour 
{
	[SerializeField] float offTime = 0.5f;
	[SerializeField] float onTime = 0.1f;

	[SerializeField] Light myLight = null;

	bool on = false;
	float time = 0;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		time += Time.deltaTime;
		if(on)
		{
			if(time >= onTime)
			{
				on = false;
				myLight.enabled = false;
				time = 0;
			}
		}
		else
		{
			if(time >= offTime)
			{
				on = true;
				myLight.enabled = true;
				time = 0;
			}
		}
	}
}
