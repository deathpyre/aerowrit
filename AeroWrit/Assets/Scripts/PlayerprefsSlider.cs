﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerprefsSlider : MonoBehaviour
{
	public string playerpref = "";
	// Use this for initialization
	void Start ()
	{
		GetComponent<Slider>().value = PlayerPrefs.GetFloat(playerpref, 0.5f);
	}

	public void SetSlider(float value)
	{
		PlayerPrefs.SetFloat(playerpref, value);
	}
}
