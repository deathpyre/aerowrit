﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class GameController : MonoBehaviour
{
	public static GameController Instance = null;

	[SerializeField] PlaneManager planeManager = null;
	public Transform targetPrefab = null;
	public Vector3 targetPosition = Vector3.zero;
	public Transform startPosition = null;
	[SerializeField] MouseLook mouselookScript = null;
	[SerializeField] GameObject deadCameraRig = null;
	[SerializeField] GameObject terrain = null;


	[Header("Screens")]
	public GameObject goScreen = null;
	public GameObject mainUiScreen = null;
	public GameObject overlayUiScreen = null;
	public GameObject pauseScreen = null;
	public GameObject optionsScreen = null;

	[Header("End Info")]
	public Text flightTime = null;
	public Text objectives = null;
	public Text distance = null;

	[Header("Tutorial/Typer")]
	public TypingText overlayTyper = null;
	public bool showingTyper = false;
	[SerializeField] int scriptStep = -1;
	public Text objectiveText = null;

	[Header("Monster")]
	[SerializeField] MonsterController monster = null;
	[SerializeField] MonsterPath swipePath = null;
	float monsterTravelledTime = 0;

	int totalObjectives = 0;

	Transform spawnedPrefab = null;
	//Bounds allowedArea = new Bounds();
	Vector2 allowedSize = new Vector2(15000, 15000);



	float timeElapsed = 0;
	float delayNeeded = 0;
	float delayWaited = 0;

	float reloadPause = 0;

	float tutorialSwipeWaited = 0;

	bool init = false;

	// Use this for initialization
	void Awake ()
	{
		objectiveText.transform.parent.gameObject.SetActive(false);
		deadCameraRig.SetActive(false);
		var needsTutorial = PlayerPrefs.GetInt("CompletedTutorial",0) == 0;
		introSent = !needsTutorial;
		terrain.SetActive(needsTutorial);
		if(!needsTutorial)
			planeManager.transform.position += new Vector3(0, 200);

		Instance = this;
		//allowedArea = new Bounds(new Vector3(2500, 300, 10000), new Vector3(20000,120,20000));
		spawnedPrefab = GameObject.Instantiate(targetPrefab, transform) as Transform;
		GenerateFirstTarget();

		goScreen.SetActive(false);
		mainUiScreen.SetActive(true);
		overlayUiScreen.SetActive(false);
		pauseScreen.SetActive(false);
		optionsScreen.SetActive(false);

		SetCursor(false);

		BlackoutScreen.Instance.FadeUp(SetScreenReady);

		if(!needsTutorial)
		{
			AddText("Hello!? Are you okay!? Some giant thing flew at your plane, and you've been drifting for a few minutes!");
			AddText("Please, collect as many of those <color=cyan>BLUE rings</color> as you can before that thing comes back!");
		}
	}

	public void SetOptions(bool on)
	{
		optionsScreen.SetActive(on);
	}

	public float volume;
	public void SetVolume(float vol)
	{
		volume = vol;
		audioSources = GameObject.FindObjectsOfType<AudioSource>().ToList();
		foreach(var source in audioSources)
			source.volume = vol;
	}
	public void SetSensitivity(float sens)
	{
		mouselookScript.sensitivityX = (sens/0.5f) * 4f;
		mouselookScript.sensitivityY = (sens/0.5f) * 4f;

		PlayerPrefs.SetFloat("mouseSensitivity", sens);
	}

	public void SetScreenReady()
	{
		screenReady = true;
	}

	void GenerateFirstTarget()
	{
		spawnedPrefab.gameObject.SetActive(true);

		targetPosition = startPosition.position;
		spawnedPrefab.position = targetPosition;
	}

	void GenerateATarget()
	{
		spawnedPrefab.gameObject.SetActive(true);
		//var randX = Random.Range(-allowedArea.size.x/2f, allowedArea.size.x/2f);
		//var randY = Random.Range(-allowedArea.size.y/2f, allowedArea.size.y/2f);
		//var randZ = Random.Range(-allowedArea.size.z/2f, allowedArea.size.z/2f);
		//var randomVector = new Vector3(randX, randY, randZ);

		//targetPosition = allowedArea.center + randomVector;
		var pos = planeManager.transform.position;
		pos.y = 0;
		targetPosition = new Vector3(Random.Range(-allowedSize.x/2f,allowedSize.x/2f), Random.Range(40, 500), Random.Range(-allowedSize.y/2f,allowedSize.y/2f));
		spawnedPrefab.position = targetPosition;
	}

	public void CollectedTarget()
	{
		totalObjectives++;
		GenerateATarget();
	}

	bool playerJustDied;
	float playerDeadWait;
	public void PlayerDied(float time, float dist)
	{
		playerDeadWait = 2;
		playerJustDied = true;

		var minutes = (int)(time / 60f);
		var remainingSeconds = time - (minutes * 60);
		flightTime.text = minutes.ToString("N0")+"m "+remainingSeconds.ToString("N0")+"s";

		objectives.text = totalObjectives.ToString("N0");

		distance.text = (dist / 1000f).ToString("N2")+"km";
	}

	public void Retry(bool fast = false)
	{
		SetCursor(false);
		if(!fast)
			BlackoutScreen.Instance.FadeDown(RetryFaded);
		else
			BlackoutScreen.Instance.FadeDownFast(RetryFadedFast);
	}

	public void RetryFaded()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		Time.timeScale = 1;
	}

	public void RetryFadedFast()
	{
		reloadPause = 5;
		audioSources = GameObject.FindObjectsOfType<AudioSource>().ToList();
		foreach(var source in audioSources)
			source.Pause();
	}

	public void Menu()
	{
		BlackoutScreen.Instance.FadeDown(MenuFaded);
	}

	public void MenuFaded()
	{
		SetCursor(true);
		SceneManager.LoadScene("MenuScene");
		Time.timeScale = 1;
	}

	public void Resume()
	{
		SetCursor(false);
		pauseScreen.SetActive(false);
		Time.timeScale = 1;
		foreach(var source in audioSources)
			source.UnPause();
	}

	public void Quit()
	{
		Application.Quit();
	}

	void SetCursor(bool on)
	{
		mouselookScript.enabled = !on;
		Cursor.lockState = on ? CursorLockMode.None : CursorLockMode.Locked;
		Cursor.visible = on;
	}

	public void SetTextOn(bool on)
	{
		mainUiScreen.SetActive(!on);
		overlayUiScreen.SetActive(on);
		showingTyper = on;
	}

	void AddText(string text)
	{
		if(delayNeeded == 0)
			SetTextOn(true);
		overlayTyper.AddText(text);
	}

	void AddText(float delay, string text)
	{
		delayWaited = 0;
		delayNeeded = delay;
		overlayTyper.AddText(text);
	}

	bool introSent = false;
	List<AudioSource> audioSources = null;
	bool screenReady;
	// Update is called once per frame
	void Update () 
	{
		if(!init)
		{
			init = true;
			SetVolume(PlayerPrefs.GetFloat("volume"));
		}
		/*monster.SetSway(false);
		monsterTravelledTime += Time.deltaTime;
		var percent = Mathf.Clamp(monsterTravelledTime / swipePath.timeToTravel, 0, 1);
		monster.transform.position = Vector3.Lerp(swipePath.transform.position, swipePath.other.position, percent);//Move monster on path
		monster.transform.LookAt(swipePath.transform);*/
		if(playerJustDied)
		{
			playerDeadWait -= Time.deltaTime;
			if(playerDeadWait <= 0)
			{
				playerJustDied = false;
				deadCameraRig.transform.position = planeManager.transform.position;
				deadCameraRig.SetActive(true);
				SetCursor(true);
				goScreen.SetActive(true);
				mainUiScreen.SetActive(false);
				overlayUiScreen.SetActive(false);
			}
		}

		if(reloadPause > 0)
		{
			reloadPause -= Time.deltaTime;
			if(reloadPause <= 0)
			{
				RetryFaded();
			}
		}
		
		if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Pause))
		{
			if(!pauseScreen.activeSelf)
			{
				SetCursor(true);
				pauseScreen.SetActive(true);
				Time.timeScale = 0;

				audioSources = GameObject.FindObjectsOfType<AudioSource>().ToList();
				foreach(var source in audioSources)
					source.Pause();
			}
			else
			{
				optionsScreen.SetActive(false);
				Resume();
			}

		}

		if(!introSent && screenReady)
		{
			planeManager.lockedFuel = true;
			timeElapsed += Time.deltaTime;
		}

		if(!introSent && timeElapsed > 3)
		{
			introSent = true;
			scriptStep = 0;
			AddText("*radio crackles* Hey! Is there anyone out there?");
			AddText("Wait, I think I see a plane! If you can hear me, type \"<b>left 20</b>\" to signal to me.");
			objectiveText.text = "•Left 20";
			objectiveText.transform.parent.gameObject.SetActive(true);
		}

		if(delayNeeded != 0)
		{
			delayWaited += Time.deltaTime;
			if(delayWaited >= delayNeeded)
			{
				delayWaited = 0;
				delayNeeded = 0;
				SetTextOn(true);
				if(scriptStep < 7)
					objectiveText.transform.parent.gameObject.SetActive(true);
			}
		}

		if(scriptStep == 4)
		{
			tutorialRing.position = planeManager.transform.position + planeManager.transform.forward * 280 + planeManager.transform.right * 95;
			tutorialRing.LookAt(planeManager.transform);
		}
		if(scriptStep == 5)
		{
			if(!tutorialRing.gameObject.activeSelf)
			{
				AddText("That added "+RingScript.fuelAmount.ToString("N0")+" fuel to your tank... but I also need you to collect these special <color=cyan>BLUE</color> packages.");
				AddText("They are filled with TONS of fuel, and supplies. You can find them on your \"<b>map</b>\" by using your \"<b>compass</b>\" now.");
				objectiveText.text = "•Map\n•Compass";
				scriptStep = 6;
			}
		}
		if(scriptStep == 7)
		{
			if(!overlayTyper.HasTextLeft())
			{
				tutorialSwipeWaited += Time.deltaTime;
				if(tutorialSwipeWaited >= 10)
				{
					scriptStep = 8;
					monster.SetSway(false);
					monster.transform.position = swipePath.transform.position;

				}
			}
		}
		else if(scriptStep == 8)
		{
			monster.transform.LookAt(swipePath.transform);
			monsterTravelledTime += Time.deltaTime;
			var percent = Mathf.Clamp(monsterTravelledTime / swipePath.timeToTravel, 0, 1);
			monster.transform.position = Vector3.Lerp(swipePath.transform.position, swipePath.other.position, percent);//Move monster on path

			if(percent >= 1)
			{
				scriptStep = 9;
				PlayerPrefs.SetInt("CompletedTutorial",1); //Complete tut
				Retry(true);
			}
		}
	}

	Transform tutorialRing;
	bool saidMap = false;
	bool saidCompass = false;
	public bool FilterText(string text)
	{
		bool allowText = scriptStep == -1;
		bool skipToEnd = false;
		if(text == "skip")
		{
			scriptStep = 8;
			skipToEnd = true;
			allowText = true;
		}

		switch(scriptStep)
		{
		case 0:
			if(text == "left 20")
			{
				objectiveText.text = "•Up 30";
				scriptStep = 1;
				AddText("Good, you can hear me! You're flying awfully low, type \"<b>up 30</b>\" to climb at 30 degrees.");
				allowText = true;
			}
			break;
		case 1:
			if(text == "up 30")
			{
				objectiveText.transform.parent.gameObject.SetActive(false);
				objectiveText.text = "•Level";
				scriptStep = 2;
				AddText(2,"Not that high! Type \"<b>level</b>\" to level out and fly flat!");
				allowText = true;
			}
			break;
		case 2:
			if(text == "level")
			{
				objectiveText.transform.parent.gameObject.SetActive(false);
				objectiveText.text = "•Fuel";
				scriptStep = 3;
				AddText(3,"Wait, how long have you been flying? Check your \"<b>fuel</b>\" to make sure you're ok.");
				allowText = true;
			}
			break;
		case 3:
			if(text == "fuel")
			{
				objectiveText.transform.parent.gameObject.SetActive(false);
				objectiveText.text = "•Right 40";
				scriptStep = 4;
				tutorialRing = RingManager.Instance.SpawnRing();
				planeManager.fuel = 1000f;
				AddText(5,"Okay, sounds like you have about 60 seconds of fuel left. Type \"<b>right 40</b>\" to fly through that <color=yellow>ring</color> and collect the fuel.");
				allowText = true;
			}
			break;
		case 4:
			if(text == "right 40")
			{
				scriptStep = 5;
				allowText = true;
			}
			break;
		case 5:
			//Handled in update
			break;
		case 6:
			if(text == "map")
			{
				allowText = true;
				saidMap = true;
			}
			if(text == "compass")
			{
				allowText = true;
				saidCompass = true;
			}

			if(saidMap && saidCompass)
			{
				objectiveText.transform.parent.gameObject.SetActive(false);
				scriptStep = 7;
				tutorialSwipeWaited = 0;
				AddText(6,"You can use these two \"<b>instruments</b>\" and one other to find your way to the next objective. Hit <b>up arrow</b> to retype your last message.");
				AddText("Don't forget to check your fuel and collect the smaller <color=yellow>rings</color> on your way! I'll keep you updated as you collect the <color=cyan>BLUE rings</color>.");
			}
			break;
		case 7:
		case 8:
			allowText = true;
			break;
		}


		return allowText;
	}
}
