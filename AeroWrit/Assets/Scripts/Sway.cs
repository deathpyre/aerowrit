﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour
{
	[SerializeField] float totalTime = 4f;
	[SerializeField] float xScale = 0.01f;
	[SerializeField] float yScale = 0.006f;
	float time = 0;
	Vector3 centerPosition = Vector3.zero;

	float leanLimit = 50;
	int leanDirection = 0;

	// Use this for initialization
	void Start ()
	{
		centerPosition = transform.localPosition;
		swayOn = true;
	}

	bool swayOn;
	// Update is called once per frame
	void Update ()
	{
		if(!swayOn)
			return;
		
		time += Time.deltaTime;

		var percent = time / totalTime;
		var percentVer = time / (totalTime*2);

		transform.localPosition = centerPosition + new Vector3(Mathf.Sin(percentVer * Mathf.PI*2) * xScale - (xScale/2f), Mathf.Sin(percent * Mathf.PI*2) * yScale - (yScale/2f), transform.localPosition.z);
	}

	public void Lean(int direction)
	{
		if(leanDirection != 0)
		{
			if(Mathf.Sign(direction) != Mathf.Sign(leanDirection))//because we flip it below, ignore if it doesnt mwatch
				return;
		}
		leanDirection -= direction;
		transform.parent.localPosition = new Vector3(0.2f * leanDirection,0);
		//transform.parent.parent.localEulerAngles = new Vector3(0, 0, leanLimit * leanDirection);
	}

	public void SetSway(bool on)
	{
		transform.localPosition = centerPosition;
		swayOn = on;
	}
}
