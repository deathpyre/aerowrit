﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatLogManager : MonoBehaviour
{
	[SerializeField] Transform textPrefab = null;

	List<Text> textObjects = new List<Text>();

	int inputIndex = -1;
	List<string> inputs = new List<string>();

	GridLayoutGroup grid = null;

	// Use this for initialization
	void Start ()
	{
		grid = GetComponent<GridLayoutGroup>();
	}

	Vector2 lastSize = Vector2.zero;

	// Update is called once per frame
	void Update ()
	{
		if(lastSize != new Vector2(Screen.width, Screen.height))
		{
			lastSize =  new Vector2(Screen.width, Screen.height);
			var scale = new Vector2(lastSize.x / 1920f, lastSize.y / 1080f); 
			grid.cellSize = new Vector2(scale.x * 600, scale.y * 40);
		}
	}

	public string GetNextInput()
	{
		inputIndex ++;
		if(inputIndex > 4)
			inputIndex = 4;

		inputIndex = Mathf.Clamp(inputIndex, -1, inputs.Count-1);
		if(inputIndex == -1)
			return "";

		return inputs[inputIndex];
	}
	public string GetPreviousInput()
	{
		inputIndex --;

		inputIndex = Mathf.Clamp(inputIndex, -1, inputs.Count-1);
		if(inputIndex == -1)
			return "";

		return inputs[inputIndex];
	}
	public void ClearInputIndex()
	{
		inputIndex = -1;
	}

	public void AddText(string words, bool bright, bool isGenerated)
	{
		if(isGenerated)
			inputs.Insert(0,words);
		
		var newText = GameObject.Instantiate(textPrefab,transform).GetComponent<Text>();
		newText.transform.SetAsFirstSibling();
		textObjects.Add(newText);
		newText.text = words;

		if(!bright)
			newText.color = newText.color - new Color(0,0,0,0.4f);

		if(textObjects.Count > 8)
		{
			GameObject.Destroy(textObjects[0].gameObject);
			textObjects.RemoveAt(0);
		}
	}
}
