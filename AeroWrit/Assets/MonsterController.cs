﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class MonsterController : MonoBehaviour
{
	[SerializeField] Sway swayScript = null;
	[SerializeField] PostProcessingProfile profile = null;
	[SerializeField] PlaneManager planeManager = null;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		var distance = Vector3.Distance(planeManager.transform.position, transform.position);
		var maxDistance = 150;
		if(distance < maxDistance)
		{
			var min = 40;
			var percent = Mathf.Clamp(distance-min,0,maxDistance-min) / (maxDistance-min);

			percent = Mathf.Clamp(percent, 0, 1f);
			percent = 1f - percent;

			if(Application.platform != RuntimePlatform.WebGLPlayer)
			{
				profile.chromaticAberration.enabled = true;
				var chrSettings = profile.chromaticAberration.settings;
				chrSettings.intensity = percent*8;
				profile.chromaticAberration.settings = chrSettings;

				profile.depthOfField.enabled = true;
				var depthSettings = profile.depthOfField.settings;
				depthSettings.focalLength = percent * 40f;
				profile.depthOfField.settings = depthSettings;

				profile.grain.enabled = true;
				var grainSettings = profile.grain.settings;
				grainSettings.intensity = percent*3;
				profile.grain.settings = grainSettings;
			}
		}
		else
		{
			profile.chromaticAberration.enabled = false;
			profile.depthOfField.enabled = false;
			profile.grain.enabled = false;
		}
	}

	public void SetSway(bool on)
	{
		swayScript.SetSway(on);
	}

	public void OnDisable()
	{
		profile.chromaticAberration.enabled = false;
		profile.depthOfField.enabled = false;
		profile.grain.enabled = false;
	}

	public void OnDestroy()
	{
		profile.chromaticAberration.enabled = false;
		profile.depthOfField.enabled = false;
		profile.grain.enabled = false;
	}
}
