﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPath : MonoBehaviour 
{
	public Transform other = null;
	public float timeToTravel = 3;
	void OnDrawGizmos() 
	{
		Gizmos.color = Color.red;
		if(other != null)
			Gizmos.DrawLine(transform.position, other.position);
	}
}
